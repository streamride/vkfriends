package com.testprofi;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testprofi.loaders.BaseLoader;
import com.testprofi.loaders.CurrentUserLoader;
import com.testprofi.models.User;

/**
 * Created by andreyzakharov on 22.06.15.
 */
public class CurrentUserActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<User> {


    public static final String PARAM_USER = "param_user";
    private static String CURRENT_USER_URL = "https://api.vk.com/method/getProfiles?uids=%s&fields=photo_big";
    private ImageView mUserBigAvatar;
    private TextView mUserNameTv;
    private User mUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_current_user);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        if (getIntent().hasExtra(CurrentUserActivity.PARAM_USER)) {
            mUser = (User)getIntent().getSerializableExtra(CurrentUserActivity.PARAM_USER);
        }

        mUserBigAvatar = (ImageView) findViewById(R.id.user_avatar_img);
        mUserNameTv = (TextView) findViewById(R.id.user_name_tv);
        mUserBigAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (mUser != null) {
            Picasso.with(this).load(mUser.photo).into(mUserBigAvatar);
            mUserNameTv.setText(mUser.firstName + " " + mUser.lastName);
            String url = String.format(CURRENT_USER_URL, mUser.uid);
            getLoaderManager().restartLoader(1, BaseLoader.withUrl(url), this);
        }
    }
    

    @Override
    public Loader<User> onCreateLoader(int id, Bundle args) {
        return new CurrentUserLoader(this, args);
    }

    @Override
    public void onLoadFinished(Loader<User> loader, User data) {
        if (data != null) {
            Picasso.with(this).load(data.photoBig).noFade().into(mUserBigAvatar);
        }
    }

    @Override
    public void onLoaderReset(Loader<User> loader) {

    }
}
