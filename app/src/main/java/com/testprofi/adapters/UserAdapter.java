package com.testprofi.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testprofi.R;
import com.testprofi.models.User;

import java.util.List;

/**
 * Created by andreyzakharov on 22.06.15.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {


    private final Context mContext;
    private final View.OnClickListener mOnClickListener;
    private List<User> mUserList;

    public UserAdapter(Context context, View.OnClickListener onClickListener) {
        mContext = context;
        mOnClickListener = onClickListener;
    }

    public void setData(List<User> userList) {
        mUserList = userList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_item, viewGroup, false);
        view.setOnClickListener(mOnClickListener);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        User user = mUserList.get(i);
        viewHolder.mUserName.setText(user.firstName + " " + user.lastName);
        Picasso.with(mContext).load(user.photo).into(viewHolder.mUserAvatar);
    }

    public User getUserByPosition(int position) {
        if (mUserList != null) {
            return mUserList.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        if (mUserList == null)
            return 0;
        return mUserList.size();
    }

    public final static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mUserAvatar;
        private final TextView mUserName;

        public ViewHolder(View itemView) {
            super(itemView);
            mUserAvatar = (ImageView)itemView.findViewById(R.id.user_avatar_img);
            mUserName = (TextView) itemView.findViewById(R.id.user_name_tv);
        }
    }
}
