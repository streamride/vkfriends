package com.testprofi;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testprofi.adapters.UserAdapter;
import com.testprofi.loaders.BaseLoader;
import com.testprofi.loaders.UserLoader;
import com.testprofi.models.User;

import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<User>> {

    private static final String USER_LIST_URL = "https://api.vk.com/method/friends.get?user_id=125930227&fields=photo";

    private RecyclerView mRecyclerView;
    private UserAdapter mUserAdapter;


    private View.OnClickListener userClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = mRecyclerView.getChildAdapterPosition(v);
            User user = mUserAdapter.getUserByPosition(position);
            View view = v.findViewById(R.id.user_avatar_img);
            if (user != null) {
                openUser(user, view);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserAdapter = new UserAdapter(getActivity(), userClickListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.user_list_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mUserAdapter);
        getLoaderManager().restartLoader(1, BaseLoader.withUrl(USER_LIST_URL), this);
        return view;
    }


    @Override
    public Loader<List<User>> onCreateLoader(int id, Bundle args) {
        return new UserLoader(getActivity(), args);
    }

    @Override
    public void onLoadFinished(Loader<List<User>> loader, List<User> data) {
        if (data != null) {
            mUserAdapter.setData(data);
            mUserAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<User>> loader) {

    }

    private void openUser(User user, View view) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), view, "avatar");
        Intent intent = new Intent(getActivity(), CurrentUserActivity.class);
        intent.putExtra(CurrentUserActivity.PARAM_USER, user);
        ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
    }
}
