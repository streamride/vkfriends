package com.testprofi.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andreyzakharov on 22.06.15.
 */
public class UserParser {


    public List<User> parseResult(String result) throws JSONException {
        JSONObject jsonObject = new JSONObject(result);
        if (jsonObject.has("response")) {

            JSONArray jsonArray = jsonObject.getJSONArray("response");
            int size = jsonArray.length();
            List<User> userList = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                JSONObject userObject = jsonArray.getJSONObject(i);
                User user = User.parseJson(userObject);
                userList.add(user);
            }
            return userList;
        }
        return null;
    }
}
