package com.testprofi.models;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by andreyzakharov on 22.06.15.
 */
public class User implements Serializable {

    public int uid;

    public String firstName;

    public String lastName;

    public String photo;

    public String photoBig;

    public static User parseJson(JSONObject jsonObject) {
        if (jsonObject != null) {
            User user = new User();
            user.uid = jsonObject.optInt("uid");
            user.firstName = jsonObject.optString("first_name");
            user.lastName = jsonObject.optString("last_name");
            user.photo = jsonObject.optString("photo");
            user.photoBig = jsonObject.optString("photo_big");
            return user;
        }
        return null;
    }


}
