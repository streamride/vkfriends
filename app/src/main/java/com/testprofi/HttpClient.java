package com.testprofi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by andreyzakharov on 22.06.15.
 */
public class HttpClient {


    public String get(String pUrl) throws IOException {
        URL url = new URL(pUrl);
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            return streamToString(connection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    private String streamToString(InputStream in) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder stringBuffer = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuffer.append(line);
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
}
