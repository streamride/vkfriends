package com.testprofi.loaders;

import android.content.Context;
import android.os.Bundle;

import com.testprofi.models.User;
import com.testprofi.models.UserParser;

import org.json.JSONException;

import java.util.List;

/**
 * Created by andreyzakharov on 22.06.15.
 */
public class UserLoader extends BaseLoader<List<User>> {


    public UserLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }


    @Override
    protected List<User> processResponse(String result) {
        UserParser userParser = new UserParser();
        List<User> userList = null;
        try {
            userList = userParser.parseResult(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userList;
    }
}
