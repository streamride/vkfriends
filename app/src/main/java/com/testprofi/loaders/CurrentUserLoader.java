package com.testprofi.loaders;

import android.content.Context;
import android.os.Bundle;

import com.testprofi.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andreyzakharov on 23.06.15.
 */
public class CurrentUserLoader extends BaseLoader<User> {

    public CurrentUserLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    @Override
    protected User processResponse(String result) {
        if (result != null) {
            try {
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.has("response")) {
                    JSONArray userObj = jsonObject.getJSONArray("response");
                    return User.parseJson(userObj.getJSONObject(0));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
