package com.testprofi.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Bundle;

import com.testprofi.HttpClient;

import java.io.IOException;

/**
 * Created by andreyzakharov on 23.06.15.
 */
public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {

    private static final String PARAM_URL = "param_url";
    private final String mUrl;

    public BaseLoader(Context context, Bundle bundle) {
        super(context);
        mUrl = bundle.getString(PARAM_URL);
    }

    public static Bundle withUrl(String url) {
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_URL, url);
        return bundle;
    }

    @Override
    public T loadInBackground() {
        HttpClient httpClient = new HttpClient();
        try {
            String result = httpClient.get(mUrl);
            return processResponse(result);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    protected abstract T processResponse(String result);
}
